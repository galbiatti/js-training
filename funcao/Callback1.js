//callback: função para outra função, e chama de volta 1 ou diversas vezes

const fabricantes = ["Mercedes", "Audi", "BMW"]

function imprimir(nome, indice) {
    console.log(`${indice + 1}. ${nome}`)
}

fabricantes.forEach(imprimir) //esse é o momento da chamada da função
fabricantes.forEach(fabricante => console.log(fabricante))/*(function(a) {
    console.log(a)
})*/