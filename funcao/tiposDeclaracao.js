console.log(soma(3, 4)) //aqui o interpretador já leu todas as informações/funções
console.log(sub(3, 4)) //sub is not defined, apenas depois pode chamá-la
console.log(mult(3, 4)) //valor atribuido apenas lá embaixo


// function declaration
function soma(x, y) { //pode-se chamá-la antes da declaração dela msm (linha 1)
    return x + y
}

// function expression
const sub = function (x, y) {
    return x - y
}
console.log(sub(3, 4))

// named function expression
const mult = function mult(x, y) { // pouco usada, mais usada quando debugga
    return x * y
}
console.log(soma(3, 4))