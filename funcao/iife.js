//IIFE -> Immediately Invoked Function Expression (função imediatamente invocada)
//fugir do escopo global!!
(function() { //função anônima
    console.log('Será executada na hora!')
    console.log('Fugindo do escopo mais abrangente!')
})()
//Tudo criado dentro dessa função é apenas dela