let dobro = function (a) {
    return 2 * a
}

dobro = (a) => { //arrow function é semre anonima 
    return 2 * a
}

dobro = a => 2 * a  //return implicito
console.log(dobro(Math.PI))

let ola = function () {
    return 'Ola'
}


ola = () => 'Olá'
ola =_ => 'Olá' //"_" mostra que tem um parâmetro
console.log(ola())