//Função construtora seria a Classe em Java! Cria-se um objeto e instancia um objeto

function Carro(velocidadeMaxima = 200, delta = 5) {
    //atributo privado
    let velocidadeAtual = 0 //sempre interessante no JS Moderno o uso do let invés do "var"

    //método publico
    this.acelerar = function () { //this ajuda a deixar algo visivel fora da função
        if (velocidadeAtual + delta <= velocidadeMaxima) {
            velocidadeAtual += delta
        } else {
            velocidadeAtual = velocidadeMaxima
        }
    }

    //método público
    this.getVelocidadeAtual = function () { //this deixa ele público, se colocasse const seria privado
        return velocidadeAtual
    }
}

const uno = new Carro //pode colocar () ou não
uno.acelerar()
console.log(uno.getVelocidadeAtual())

const ferrari = new Carro(350, 20)
ferrari.acelerar()
ferrari.acelerar()
ferrari.acelerar()
console.log(ferrari.getVelocidadeAtual())

console.log(typeof Carro)
console.log(typeof ferrari)