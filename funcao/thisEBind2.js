function Pessoa() {
    this.idade = 0

    const self = this //Essa constante nunca mudará e mudamos o thiis.idade por self
    setInterval(function (){ //essa função recebe uma função e recebe um intervalo
        /*this*/self.idade++ //Jeito de "driblar" o this colocando-o como constante dentro da função.
        console.log(/*this*/self.idade)
    }/*.bind(this)*/, 1000) //A cada 1000ms ele modifica... -- Esse this dentro do bind aponta para a pessoa criada ali em cima
}

new Pessoa //CTRL ALT M para parar o código