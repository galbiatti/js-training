const notas = [7.7, 6.5, 5.2, 8.9, 3.6, 7.1, 9.0]

// Sem callback
const notasBaixas1 = []
for (let i in notas) {
    if (notas[i] <7) {
        notasBaixas1.push(notas[i])
    }
}

console.log(notasBaixas1)

// Com callback
const notasBaixas2 = notas.filter(function(nota) {
//pra cada elemento, se for verdadeiro entra no array
//filter() nao altera o "notas"!!
    return nota < 7
})

console.log(notasBaixas2)

const notasMenorQue7 = nota => nota <7
const notasBaixas3 = notas.filter(notasMenorQue7)
console.log(notasBaixas3)

/*A ideia do callback é utilizar a função filter que para
cada elemento ele verifica se é verdadeiro e se for ele inclui no array*/