//São funções sem nome --sempre será anônima se for arrow
const soma = function (x,y) {
    return x + y
}

const imprimirResultado = function (a, b, operacao = soma){ //se precisar ele usa o param "operacao"
    console.log(operacao(a, b))
}

imprimirResultado(3, 4)
imprimirResultado(3, 4, soma)
imprimirResultado(3, 4, function(x, y) { //aqui ele nao usa a operação soma
    return x - y
})
imprimirResultado(3, 4, (x, y) => x * y)

const pessoa = {
    falar: function () {
        console.log('Opa')
    }
}

pessoa.falar()