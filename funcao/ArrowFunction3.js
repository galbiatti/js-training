/*this na arrow function se mantém
na função normal this aponto pro global
portanto vem o lado bom do arrowfunction, this não é mais global*/
let comparaComThis = function (param) {
    console.log(this === param)
}

comparaComThis(global)  //no Browser o "global" é "window"

const obj = {}
comparaComThis = comparaComThis.bind(obj)
comparaComThis(global)
comparaComThis(obj) //por conta do bind ele aponta pro objeto

/*sobre essas três linhas: a função foi definida dentro de um
módulo no Node, e cada arquivo node representa um módulo,
portanto o "this" seria o próprio módulo*/
let comparaComThisArrow = param => console.log(this === param)
comparaComThisArrow(global) // não é mais global pra arrow function
comparaComThisArrow(module.exports) //

//Arrow Function mantém o this apontando pra o que a função foi escrita: FALSO
comparaComThisArrow = comparaComThisArrow.bind(obj)
comparaComThisArrow(obj)
comparaComThisArrow(module.exports) 