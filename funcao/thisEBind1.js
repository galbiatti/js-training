const pessoa = { //criação de um objeto com atributo e função, tudo do es2015
    saudacao: 'Bom dia!',
    falar() {
        console.log(this.saudacao) //só é possível acessar o "saudacao" com o uso do this dentro do objeto pessoa
    }
}

pessoa.falar()
const falar = pessoa.falar
falar() //conflito entre paradigmas funcional e Orientado a Objeto

const falarDePessoa = pessoa.falar.bind(pessoa) //vc passa o objeto no qual vc quer que resolva o this 
falarDePessoa() //agora sim ele vai buscar a saudacao de "pessoa"
//Quando usar o this, será o objeto relacionado

const falar2 = pessoa.falar //pessoa.falar nao foi alterado com o bind
falar2() 