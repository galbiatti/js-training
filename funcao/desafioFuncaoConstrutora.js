function Pessoa(nome) {
    this.nome = nome

    this.falar = function () {
        console.log(`Meu nome é ${this.nome}`)
    }
}

const p1 = new Pessoa('Joaquim')
p1.falar()

//Duas formas de criar instâncias, com factory e função construtora