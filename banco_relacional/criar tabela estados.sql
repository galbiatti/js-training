-- Criando tabela Estado
create table estados (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT, -- cria o primeiro e os proximos id sao incrementados automaticamente
	nome VARCHAR(45) NOT NULL,
    sigla VARCHAR(2) NOT NULL,
    regiao ENUM('Norte', 'Nordeste', 'Centro-Oeste', 'Sudeste', 'Sul') NOT NULL,-- valores obrigatórios para campo, assim como no Java
    populacao DECIMAL(5,2) NOT NULL,
    primary key (id),
    UNIQUE KEY (nome),
    UNIQUE KEY (sigla)
    );