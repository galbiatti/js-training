let p = new Promise(function(resolve){
    resolve(['Ana', 'Bia', 'Carlos',"Daniel"])
})
    .then(valor => valor[0])
    .then(primeiro => primeiro[0])
    .then(letra => letra.toLowerCase())
    .then(letraMinuscula => console.log(letraMinuscula))