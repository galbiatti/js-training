// Código não executável
//-------------------------------//

// paradigma procedure: procedimento, um trecho de código em que pode se aproveitar...hoje temos a função
processamento(valor1, valor2, valor3) //função

// OO
objeto = {
    valor1,
    valor2,
    valor3, //os atributos
    processamento() { //funções
        //...
    }
}

objeto.processamento() // foco passou a ser o objeto

// Princípios importantes
/* ABSTRAÇÃO:
    Como traduzir esse objeto para o mundo real
   ENCAPSULAMENTO:
    Mostrar pra quem se deve, protegê-lo
   HERANÇA:
    Receber atributos e comportamentos do "pai"
   POLIMORFISMO:
    Várias formas através de um genérico*/