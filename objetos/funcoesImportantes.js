const pessoa = {
    nome: 'Rebeca',
    idade: 2,
    peso: 13
}

console.log(Object.keys(pessoa)) //pega as chaves de um objeto
console.log(Object.values(pessoa))
console.log(Object.entries(pessoa)) //retorna um array dos elementos

Object.entries(pessoa).forEach(e => { //e: elementos
    console.log(`${e[0]}: ${e[1]}`)
})

Object.entries(pessoa).forEach(([chave, valor]) => { //destructuring...
    console.log(`${chave}: ${valor}`)
})

Object.defineProperty(pessoa, 'dataNascimento', {
    enumerable: true,
    writable: false,
    value: '01/01/2019'
})

pessoa.dataNascimento = '01/01/2017'
console.log(pessoa.dataNascimento) //mantém o 2019, não é sobrescrita conforme linha 21
console.log(Object.keys(pessoa)) //se colocar false na linha 20, dataNascimento não printa

//Object.assign {ECMAScript 2015}
const dest = {a: 1} //dest é destino...
const o1 = {b: 2}
const o2 = {c: 3, a: 4}
const obj = Object.assign(dest, o1, o2) //dest recebe, valor será sobrescrito


Object.freeze(obj)
obj.c = 1234
console.log(obj)
//é possível fazer um SQL e inserir no banco de dados, de acordo com esta aula