// Object.preventExtension : vc nao consegue aumentar nº atributos
const produto = Object.preventExtensions({
    nome: 'Qualquer',
    preco : 1.99,
    tag: 'promoção'
})
console.log('Extensível:', Object.isExtensions(produto))

produto.nome = 'Borracha'
produto.descricao = 'Borracha escolar branca'
delete produto.tag
console.log(produto)

// Object.seal: nao adiciona nem exclui, mas modifica
const pessoa = {nome: 'Juliana', idade: 35}
Object.seal(pessoa)
console.log('Selado', Object.isSealed(pessoa))

pessoa.sobrenome = 'Silva'
delete pessoa.nome
pessoa.idade = 29
console.log(pessoa)

// Object.freeze : selado com valores contantes