console.log(typeof String)
console.log(typeof Array)
console.log(typeof Object)

String.prototype.reverse = function () {
    return this.split('').reverse().join('') //gera um array, separa, inverte e junta de novo
}

console.log('Escola Cod3r'.reverse())

Array.prototype.first = function () {
    return this[0] //acesso array ou string a partir do protótipo através do THIS
}

console.log([1, 2, 3, 4, 5].first())
console.log(['a', 'b', 'c'].first())

String.prototype.toString = function () { //EVITAR!!! Substituir comportamentos globais
    return 'Lascou tudo'
}

console.log('Escola Cod3r'.reverse())