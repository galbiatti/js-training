class Avo {
    constructor(sobrenome) {
        this.sobrenome = sobrenome
    }
}

class Pai extends Avo { //extends: relação de protótipo
    constructor(sobrenome, profissao = 'Professor') {
        super(sobrenome) // Avo é superclasse de Pai
        this.profissao = profissao
    }
}

class Filho extends Pai {
    constructor() {
        super('Sousa')
    }
}

const filho = new Filho
console.log(filho)