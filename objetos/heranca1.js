/*objetivo da herança é aproveitamento de código

OBS: priorizar a composição: um objeto pode ser composto por vários objetos*/
const ferrari = {
    modelo: 'F40',
    velMax: 324
}

const volvo = {
    modelo: 'V40',
    velMax: 200
}
//__proto__ está disponível no Node e no Browser
console.log(ferrari.prototype) // objeto não tem o atributo prototype
console.log(ferrari.__proto__) //acessa o super objeto, o pai da "ferrari", se acha em uma das cadeiras de prototipo ele printa
console.log(ferrari.__proto__ === Object.prototype) //TRUE
console.log(volvo.__proto__ === Object.prototype)
console.log(Object.prototype.__proto__) // vai pra apontar pra null
console.log(Object.prototype.__proto__ === null)

function MeuObjeto() {}
console.log(typeof Object, typeof MeuObjeto)
console.log(Object.prototype, MeuObjeto.prototype) //lembrando que os dois são funções
console.log(ferrari.prototype) // objeto não tem o atributo prototype