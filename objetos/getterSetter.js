const sequencia = {
    _valor: 1, //convenção que _ signifa que é acessada internamente
    get valor() { return this._valor++ },
    set valor(valor) {
        if (valor > this._valor) {
            this._valor = valor
        }
    }
}

console.log(sequencia.valor, sequencia._valor) //internamente chama o metodo get e set
sequencia._valor = 1000
console.log(sequencia._valor, sequencia._valor)
sequencia._valor = 900
console.log(sequencia._valor, sequencia._valor)

//conceito antes do ES2015 - um encapsulamento diferente//