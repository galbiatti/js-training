// pessoa -> endereço de memória 123 -> {...}
const pessoa = { nome: 'Joao'}
pessoa.nome = 'Pedro' //pessoa está no 123...
console.log(pessoa)

//pessoa <- endereço 456 <- {...} pessoa aponta para novo endereço, mas o objeto está em outro
// pessoa = {nome: 'Ana'}
//não se pode atribuir um outro objeto para "pessoa"

Object.freeze(pessoa) //objeto vira constante

pessoa.nome = 'Maria'
pessoa.end = 'Rua ABC'
delete pessoa.nome
console.log(pessoa.nome)
console.log(pessoa)

const pessoaConstante = Object.freeze({nome: 'Joao'})
pessoaConstante.nome = 'Maria'
console.log(pessoaConstante)