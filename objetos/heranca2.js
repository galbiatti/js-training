// Cadeia de prototipos (prototype chain)
Object.prototype.atr0 = '0' //Evitar essa manipulação!! NÃO RECOMENDADO!
const avo = {atr1: 'A'}
const pai = {__proto__: avo, atr2: 'B', atr3: '3'}
const filho= {__proto__: pai, atr3: 'C'} //prototipo do avo é o Object.prototype
console.log(filho.atr0, filho.atr1, filho.atr2, filho.atr3)//quando não tem => undefined
//atr3 possui no filho, portanto procura o mais perto, dizemos que o atr3 do filho "sombreou" o atr do pai

const carro = {
    velAtual: 0,
    velMax: 200,
    aceleraMais(delta) {
        if(this.velAtual + delta <= this.velMax){
            this.velAtual += delta
        } else {
            this.velAtual = this.velMax
        }
    },
    status() {
        return `${this.velAtual}Km/h de ${this.velMax}Km/h`
    }
}

const ferrari = {
    modelo: 'F40',
    velMax: 324 //SHADOWING: variavel local sombrea variáveis de escopo mais abrangente
}

const volvo = {
    modelo: 'V40',
    status() {
        return `${this.modelo}: ${super.status()}` //sombreando status - super procura o do protótipo
    }
}

Object.setPrototypeOf(ferrari, carro) //estabeleceu a relação - ferrari tem carro de prototipo --Qualquer JS
Object.setPrototypeOf(volvo, carro)

console.log(ferrari)
console.log(volvo) //por padrão lê o que tem no objeto, ainda nao chama no protoripo

volvo.aceleraMais(100)
console.log(volvo.status())

ferrari.aceleraMais(300)
console.log(ferrari.status())

//de acordo com esse exemplo podemos definir 2 tipos para prototipo: dentro do objeto ou setPrototype...