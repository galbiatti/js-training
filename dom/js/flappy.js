function novoElemento(tagName, className) {
    const elem = document.createElement(tagName)
    elem.className = className
    return elem;
}

function Barreira(reversa = false) {
    this.elemento = novoElemento('div', 'barreira')

    const borda = novoElemento('div', 'borda')
    const corpo = novoElemento('div', 'corpo')
    //aqui é para adicionar a barreira, se for TRUE primeiro vem o corpo e depois borda, e FALSE o contrário
    this.elemento.appendChild(reversa ? corpo : borda)
    this.elemento.appendChild(reversa ? borda : corpo)

    this.setAltura = altura => corpo.style.height = `${altura}px`
}

//para testes
// const b = new Barreira(true);
// b.setAltura(300);
// document.querySelector('[wm-flappy]').appendChild(b.elemento);

/* OBS: UTILIZAMOS O THIS NAS FUNÇÕES POIS ESTARÃO DISPONÍVEIS FORA DA FUNÇÃO, 
DESTA FORMA CONSEGUIMOS CHAMAR UMA FUNÇÃO QUE ESTÁ DENTRO DE OUTRA FUNÇÃO */

function parDeBarreiras(altura, abertura, x) {
    this.elemento = novoElemento('div', 'par-de-barreiras')

    //se fazemos
    //const superior = new Barreira(true) <- estará disponível só dentro desta função construtora
    this.superior = new Barreira(true)
    this.inferior = new Barreira(false)

    //estamos adicionando no elemento o par de barreira superior e depois inferior
    this.elemento.appendChild(this.superior.elemento)
    this.elemento.appendChild(this.inferior.elemento)

    this.sortearAbertura = () => {
        const alturaSuperior = Math.random() * (altura - abertura)
        const alturaInferior = altura - abertura - alturaSuperior
        this.superior.setAltura(alturaSuperior)
        this.inferior.setAltura(alturaInferior)
    }

    this.getX = () => parseInt(this.elemento.style.left.split('px')[0])
    this.setX = x => this.elemento.style.left = `${x}px`
    this.getLargura = () => this.elemento.clientWidth

    this.sortearAbertura()
    this.setX(x)
}

// const b = new parDeBarreiras(700, 200, 400)
// //temos que dar obrigatoriamente o append do b.elemento, pois o elemento é o elemento DOM
// document.querySelector('[wm-flappy]').appendChild(b.elemento)

//aqui criando as barreiras pro jogo, lembrando que estão fora do ambiente azul do jogo,

// para poderem vir aparecendo conforma passa o tempo
function Barreiras(altura, largura, abertura, espaco, notificarPonto) {
    this.pares = [
        new parDeBarreiras(altura, abertura, largura),
        new parDeBarreiras(altura, abertura, largura + espaco),
        new parDeBarreiras(altura, abertura, largura + espaco * 2),
        new parDeBarreiras(altura, abertura, largura + espaco * 3)
    ]
    //de quanto em quantos pixels as barreiras vão se deslocar...
    const deslocamento = 3
    this.animar = () => {
        this.pares.forEach(par => {
            par.setX(par.getX() - deslocamento)

            //quando o elemento sair da tela do jogo...nosso X é o 'left'
            if (par.getX() < -par.getLargura()) {
                //aqui, quando o x zerar, temos que somar 0 + pixels para ele ir pro lado direito da tela para começar tudo de novo...
                par.setX(par.getX() + espaco * this.pares.length)
                //se quiser manter as posições das barreiras que virão é só comentar a linha de baixo
                par.sortearAbertura()
            }

            const meio = largura / 2
            const cruzouOMeio = par.getX() + deslocamento >= meio && par.getX() < meio
            if (cruzouOMeio) notificarPonto()
        })
    }
}

function Passaro(alturaJogo) {
    let voando = false

    this.elemento = novoElemento('img', 'passaro')
    this.elemento.src = 'imgs/passaro.png'

    this.getY = () => parseInt(this.elemento.style.bottom.split('px')[0])
    this.setY = y => this.elemento.style.bottom = `${y}px`

    window.onkeydown = e => voando = true
    window.onkeyup = e => voando = false

    this.animar = () => {
        const novoY = this.getY() + (voando ? 8 : -5)
        const alturaMaxima = alturaJogo - this.elemento.clientHeight

        if (novoY <= 0) {
            this.setY(0)
        } else if (novoY >= alturaMaxima) {
            this.setY(alturaMaxima)
        } else {
            this.setY(novoY)
        }
    }

    this.setY(alturaJogo / 2)
}

//outro teste
// const barreiras = new Barreiras(700, 1200, 200, 400)
// const passaro = new Passaro(700)
// const areaDoJogo = document.querySelector('[wm-flappy]')

// areaDoJogo.appendChild(passaro.elemento)
// areaDoJogo.appendChild(new Progresso().elmento)
// barreiras.pares.forEach(par => areaDoJogo.appendChild(par.elemento))
// setInterval(() => {
//     barreiras.animar()
//     passaro.animar()
// }, 20)
function Progresso() {
    this.elemento = novoElemento('span', 'progresso')
    this.atualizarPontos = pontos => {
        this.elemento.innerHTML = pontos
    }
}

function estaoSobrepostos(elementoA, elementoB) {
    //o retângulo associado ao elemento
    const a = elementoA.getBoundingClientRect()
    const b = elementoB.getBoundingClientRect()

    //nesta parte, estamos analizando se os elementos "passaram" um do outro em horizontal e vertical
    //para cada eixo temos duas opções, quando o cada elemento está na frente do outro
    const horizontal = a.left + a.width >= b.left && b.left + b.width >= a.left
    const vertical = a.top + a.height >= b.top && b.top + b.height >= a.top
    return horizontal && vertical
}

function colidiu(passaro, barreiras) {
    let colidiu = false
    barreiras.pares.forEach(parDeBarreiras => {
        if (!colidiu) {
            const superior = parDeBarreiras.superior.elemento
            const inferior = parDeBarreiras.inferior.elemento
            colidiu = estaoSobrepostos(passaro.elemento, superior) || estaoSobrepostos(passaro.elemento, inferior)
        }
    })

    return colidiu
}

function FlappyBird() {
    let pontos = 0

    const areaDoJogo = document.querySelector('[wm-flappy]')
    const altura = areaDoJogo.clientHeight
    const largura = areaDoJogo.clientWidth

    const progresso = new Progresso()
    const barreiras = new Barreiras(altura, largura, 200, 400,
        () => progresso.atualizarPontos(++pontos))
    const passaro = new Passaro(altura)

    areaDoJogo.appendChild(progresso.elemento)
    areaDoJogo.appendChild(passaro.elemento)
    barreiras.pares.forEach(par => areaDoJogo.appendChild(par.elemento))

    this.start = () => {
        //loop do jogo
        const temporizador = setInterval(() => {
            barreiras.animar()
            passaro.animar()
            if (colidiu(passaro, barreiras)) {
                clearInterval(temporizador)
            }
        }, 10)
    }
}

new FlappyBird().start()