/*Faça um algoritmo que calcule o fatorial de um número.*/
function fatorial(numero) {
    
    if (numero < 0) {
        return 'Deve ser maior ou igual a zero'
        } else if ((numero == 0) || (numero == 1)){
        return 1;
        } else {
            var numeroFatorial = 1
            for (i = numero; i > 1 ; i--) {
            numeroFatorial = numeroFatorial * i;
            }   
        return numeroFatorial;
        }
    }
console.log(fatorial(4))