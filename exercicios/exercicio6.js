/*Elabore duas funções que recebem três parâmetros: capital inicial, taxa de juros e tempo de aplicação. A
primeira função retornará o montante da aplicação financeira sob o regime de juros simples e a segunda
retornará o valor da aplicação sob o regime de juros compostos.*/

function jurosSimples (capital, juros, tempo) {
    montante1 = capital + (capital * tempo * juros) 
    return console.log(`O valor do montante em simples é:R$ `+montante1.toFixed(2).replace('.' , ','))
}

jurosCompostos = (capital, juros, tempo) => {
    montante2 = (capital * Math.pow((1 + juros),tempo))
    return console.log(`O valor do montante em compostos é:R$ `+montante2.toFixed(2).replace('.' , ','))
}

jurosSimples(1250.15, 0.04, 5)
jurosCompostos(1250.15, 0.04, 5)
