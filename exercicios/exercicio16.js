/*Utilizando a estrutura do Switch faça um programa que simule uma calculadora básica. O programa recebe
como parâmetros dois valores numéricos e uma string referente à operação e a realize com os valores
numéricos na ordem que foram inseridos. Por exemplo: calculadora (2, ‘+’, 3). A função efetuará a soma de 2 e
3. Dica: Os sinais das operações são: ‘+’. ‘-’, ‘*’ e ‘/’. Crie um caso default para operações inválidas.*/
function divisao (a, op, b){
    switch (op) {
        case '+':
            return a + b;
            break;
        case '-':
            return a - b;
            break;
        case '*':
            return a * b;
        case '/':
            if (a == 0){
                return 0;
            } else if (b == 0){
                return 'Jamais dividirás por zero!';
            } else {
                return a / b;
            }
                break;
        default:
            return 'Favor colocar uma operação válida!'
        }
}

console.log(divisao(5,'+',5))
console.log(divisao(5,'-',5))
console.log(divisao(5,'*',5))
console.log(divisao(5,'/',5))
console.log(divisao(0,'/',5))
console.log(divisao(5,'/',0))
console.log(divisao(5,'=',0))