//Crie uma função que recebe dois parâmetros, base e expoente, e retorne a base elevada ao expoente.

function exponenciacao (x, y) {
    return console.log(`${x} elevado a ${y} é:`, Math.pow(x, y))
}

exponenciacao(2,3)

//método novo
function exponenciacao1 (x, y) {
    return console.log(`${x} elevado a ${y} é:`, x ** y)
}

exponenciacao1(2,3)