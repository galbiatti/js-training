/*Um funcionário irá receber um aumento de acordo com o seu plano de
trabalho, de acordo com a tabela abaixo:
    __________________
    | Plano | Aumento|
    |   A   |  10%   |
    |   B   |  15%   |
    |___C___|__20%___|

Faça uma função que leia o plano de trabalho e o salário atual de um funcionário e calcula e imprime o seu
novo salário. Use a estrutura switch e faça um caso default que indique que o plano é inválido.*/

function AumentoSalario (salario, plano) {
    switch (plano) {
        case 'A':
            return salarioFinal = salario * (1.1)
            break;
        case 'B':
            return salarioFinal = salario * (1.15)
            break;
        case 'C':
            return salarioFinal = salario * (1.2)
            break;
        default:
            return 'Este plano é inválio!'
    }
}
console.log(AumentoSalario(1000, 'A'))
console.log(AumentoSalario(1000, 'B'))
console.log(AumentoSalario(1000, 'C'))
console.log(AumentoSalario(1000, 'D'))