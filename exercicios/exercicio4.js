/*Crie uma função que irá receber dois valores, o dividendo e o divisor. A função deverá imprimir o resultado
e o resto da divisão destes dois valores.*/

divisao = (dividendo, divisor) => { 
    resto = dividendo % divisor
    resultado = Math.floor(dividendo / divisor)
    return console.log(`O resultado da divisão é: ${resultado} e o resto é: ${resto}`)
}

divisao(5, 2)