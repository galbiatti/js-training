const pilotos = ['Vettel', 'Alonso', 'Raikkonen', 'Massa']
pilotos.pop() //Massa quebrou o carro :/ Remove ultimo elemento
console.log(pilotos)

pilotos.push('Verstappen') //coloca no final
console.log(pilotos)

pilotos.shift()//tira da primeira posição
console.log(pilotos)

pilotos.unshift('Hamilton') // adicionar na primeira
console.log(pilotos)

//adicionar
pilotos.splice(2, 0, 'Bottas', 'Massa')
console.log(pilotos)

//remover
pilotos.splice(3, 1)
console.log(pilotos)

const algunsPilotos1 = pilotos.slice(2) // novo array a partir do parâmetro
console.log(algunsPilotos1)

const algunsPilotos2 = pilotos.slice(1, 4) //índice 4 não entra (O ÚLTIMO)
console.log(algunsPilotos2)