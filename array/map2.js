const carrinho = [
    '{ "nome": "Borracha", "preco": 3.45 }', //formato JSON
    '{ "nome": "Caderno", "preco": 13.90 }',
    '{ "nome": "Kit de Lapis", "preco": 41.22 }',
    '{ "nome": "Caneta", "preco": 7.50 }'
]

//retornar um array apenas com preços

const paraObjeto = json => JSON.parse(json) //transforma tudo em objeto
const apenasPreco = produto => produto.preco // apenas o preço

const resultado = carrinho.map(paraObjeto).map(apenasPreco)
console.log(resultado)