const nums = [1, 2, 3, 4, 5]

// For com propósito...internamente do map tem o for e transforma cada elemento do array, gera sempre um novo array
let resultado = nums.map(function(e) {
    return e * 2
})

console.log(resultado, nums)

const soma10 = e => e + 10 //return implícito do arrowfunction
const triplo = e => e * 3
const paraDinheiro = e => `R$ ${parseFloat(e).toFixed(2).replace('.', ',')}`

resultado = nums.map(soma10).map(triplo).map(paraDinheiro)
console.log(resultado)