//array é um objeto em JS! É dinâmico, sendo uma estrutura onde pode se colocar várias coisas (heterogêneas, diferentes das outras linguagens que são homogêneas)
console.log(typeof Array, typeof new Array, typeof [])// função - objeto - objeto
let aprovados = new Array('Bia', 'Carlos', 'Ana') //não muito recomendada esse jeito
console.log(aprovados)

aprovados = ['Bia', 'Carlos', 'Ana']
console.log(aprovados[0])
console.log(aprovados[1])
console.log(aprovados[2])
console.log(aprovados[3]) //undefined (outras linguagens = ERRO)

aprovados [3] = 'Paulo'
aprovados.push('Abia')
console.log(aprovados.length)

aprovados[9] = 'Rafael'
console.log(aprovados.length)
console.log(aprovados[8] === undefined)
console.log(aprovados)

aprovados.sort()
console.log(aprovados)

delete aprovados[1] //vira undefined
console.log(aprovados[1])
console.log(aprovados[2])

aprovados = ['Bia', 'Carlos', 'Ana']
aprovados.splice(1, 1, 'Elemento1', 'Elemento2')// splice = adiciona itens num índice, remover, remover e adicionar
//(1, 1) primeiro é o índice e o segundo quantos vai excluir a partir do índice e depois adicionar a partir do índice
console.log(aprovados)

/*OBS: temos um array const, o que acontece: os dados não são constantes pois eles apontam para um endereço de memória, e este endereço nao pode ser modificado,
    mas os elementos podem ser alterados, mesmo princípio do "const objeto"*/