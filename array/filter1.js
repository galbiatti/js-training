const produtos = [
    { nome: 'Notebook', preco: 2499, fragil: true},
    { nome: 'iPad Pro', preco: 4199, fragil: true},
    { nome: 'Copo de Vidro', preco: 12.99, fragil: true},
    { nome: 'Copo de Plastico', preco: 18.99, fragil: false}
]

console.log(produtos.filter(function(p) { 
    return true // se false ele sai do array, se true printa tudo
    //return p.preco > 10
}))

const caro = produto => produto.preco >= 500
const fragil = produto => produto.fragil //não precisa tratar como == true pois o fragil já é true/false

console.log(produtos.filter(caro).filter(fragil))