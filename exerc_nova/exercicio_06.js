/*Elabore duas funções que recebem três parâmetros: capital inicial, taxa de juros e tempo de aplicação. A
primeira função retornará o montante da aplicação financeira sob o regime de juros simples e a segunda
retornará o valor da aplicação sob o regime de juros compostos.*/

// Juros simples: J = C*i*t
// Juros compostos: M = C*(1 - i)^t J = M - C

const jurosSimples = (c, i, t) => {
  let montante = c*((i*t)+1)

  console.log(montante)
}

const jurosCompostos = (c, i, t) => {
  montante = c*Math.pow((1 - i),t)
 
  console.log(montante)
}

jurosSimples(1000, 0.1, 6)
jurosCompostos(1000, 0.1, 6)