/* Crie uma função que dado dois valores (passados como parâmetros) mostre no console a soma, subtração,
multiplicação e divisão desses valores.*/

const func = (a ,b) => {
  console.log("Soma: ", a + b)
  console.log("Subtração: ", a - b)
  console.log("Multiplicação: ", a * b)
  if (a < b) {
    a = b
    b = a
  }
  console.log("Divisão: ", a / b)
}
func(2,1)
func(2,2)