/*Escreva um algoritmo que percorre um vetor de inteiros e defina o maior e menor valor dentro do vetor.*/
const vetores = (vetor) => {
    let maior = 0
    let menor = 9999999999

    for (let i = 0; i < vetor.length; i++) {

        if (vetor[i] > maior) {
            maior = vetor[i]
        } 
        if (vetor[i] < menor) {
            menor = vetor[i]
        }
    }
        console.log(`O maior resultado é ${maior}`)
        console.log(`O menor resultado é ${menor}`)

}

vetor = [10, 2, 3, 4, 500, 60]
vetores(vetor)
