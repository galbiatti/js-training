/*Faça um algoritmo que calcule o fatorial de um número.*/


const fatorial = (numero) => {
    if ((numero == 0) || (numero == 1)) {
        return 1;
    } else {
        var fatorial = 1
        for (let i = numero; i > 1; i--) {
            fatorial = fatorial * i;          
        }
        
        return fatorial;
    }
}

console.log(fatorial(4))