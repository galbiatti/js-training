/*Crie uma função que irá receber dois valores, o dividendo e o divisor. A função deverá imprimir o resultado
e o resto da divisão destes dois valores.*/

const divisao = (a, b) => {
  if (a < b) {
    let x
    x = a
    a = b
    b = x    
  }
  console.log("Resultado da divisão: ", Math.floor(a /b))
  console.log("Resto: ", a % b)
}

divisao(1,2)
divisao(3,2)
divisao(5,2)