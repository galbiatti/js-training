const nome = 'Roberto'
const concatenacao = 'Olá '+ nome + '!'
const template = `
        Olá
        ${nome}!` //pega o valor da constante

console.log(concatenacao, template)
console.log(`1 + 1 = ${1 + 1}`)

const up = texto => texto.toUpperCase()
console.log(`Ei... ${up('cuidado')}!`) //ele chama a função up na expressão do 'cuidado'