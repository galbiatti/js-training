const escola = "Cod3r"

console.log(escola.charAt(4)) //mostra o char neste posição
console.log(escola.charAt(5)) //nao encontra, portanto mostra como vazio
console.log(escola.charCodeAt(3)) //começa com 0,1,2,3 sendo assim o próprio numero 3, e procura na tabela ASCI/UNICODE

console.log(escola.indexOf('3')) //qual a posição do número 3 dentro no char?
console.log(escola.substring(1)) //mostra sem  primeiro dígito
console.log(escola.substring(0, 3)) //mostra até o 2

console.log("Escola ".concat(escola).concat('!')) //ambas podem ser aspas duplas ou simples
console.log("Escola " + escola +'!') //mesma coisa que em cima
console.log(escola.replace(3, 'e'))  //muda o 3 por e

console.log('Ana,Pedro,Joao'.split(',')) //quebra a string em várias