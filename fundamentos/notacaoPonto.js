console.log(typeof console)
console.log(Math.ceil(9.1))
const obj1 = {}
obj1.nome = 'Bola' //criou dinamicamente
console.log(obj1.nome)
obj1['nome'] = 'Bola2'
console.log(obj1.nome)
function Obj(nome) { //Função funciona como Classe em POO
    this.nome = nome //palavra this ele fica visível publicamnte
    this.exec = function() {
        console.log('Exec...')
    }
}

const obj2 = new Obj('Cadeira')
const obj3 = new Obj('Mesa')
console.log(obj2.nome)
console.log(obj3.nome)
obj3.exec()