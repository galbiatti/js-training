//par nome/valor
const saudacao = 'opa' //contexto léxico: local onde sua variável foi fisicamente definida no seu código

function exec(){
    const saudacao = 'aew' //contexto léxico 2
    return saudacao
}

//Objetos são grupos aninhados de pares nome/valor
const cliente = {
    nome: 'Pedro',
    idade: 32,
    peso: 90,
    endereco: {
        logradouro: 'Rua Sion',
        numero: 152
    }
}

console.log(saudacao)
console.log(exec())
console.log(cliente)