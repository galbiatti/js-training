//Função sem retorno
function imprimirSoma(a, b) { //função funciona como Classe em POO
    console.log(a +b)
}

imprimirSoma(2, 3)
imprimirSoma(2) //b fica como undefined
imprimirSoma(2, 3, 4, 5, 6, 7, 8) //soma apenas os primeiros
imprimirSoma()

//Função com retorno
function soma(a, b = 0){
    return a + b
}
console.log(soma(2,3))
console.log(soma(2))
console.log(soma()) //a fica como undefined