const prod1 = {}
prod1.nome = 'Celular' //nome é criado dinamicamente neste caso
prod1.preco = 4999,90
prod1['desconto_legal'] = 0.40 //evitar atributos com espaço

console.log(prod1)

const prod2 = {
    nome: 'Camisa_Polo',
    preco: 79.90    
}

console.log(prod2)

const prod3 = {
    nome: 'Camiseta',
    preco: 49.90,
    obj: {            //obj aparece 2x, porém cada um está dentro de outro objeto, portanto pode
        blabla: 1,
        obj: {
            blabla: 2 //objetos ninhados
        }
    }
}

console.log(prod3)