let valor //não inicializada
console.log(valor) //undefined
//console.log(valor1) //valor is not defined, pois nem declarado foi

valor = null //ausencia de valor - nao aponta para memória
console.log(valor)
// console.log(valor.toString()) //Erro

const produto = {}
console.log(produto.preco) //preco undefined
console.log(produto)

produto.preco = 3.50
console.log(produto)

produto.preco = undefined //evitar atribuir undefined
console.log(!!produto.preco)
console.log(produto)

produto.preco = null //sem preço
console.log(produto)
console.log(!!produto.preco)